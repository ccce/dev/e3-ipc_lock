#include <sys/mman.h>
#include <sys/errno.h>
#include <dbDefs.h>
#include <subRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

static long cap_ipc_lock_Init(struct subRecord *psub)
{
    psub->val = 42;

    return 0;
}


static long cap_ipc_lock_Process(struct subRecord *psub)
{
    psub->val = mlockall(MCL_CURRENT | MCL_FUTURE);
    if (psub->val)
        psub->e = errno;

    return 0;
}

epicsRegisterFunction(cap_ipc_lock_Init);
epicsRegisterFunction(cap_ipc_lock_Process);
